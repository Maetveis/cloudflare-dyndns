"""Cloudflare dynamic dns"""

import sys
from importlib import metadata as importlib_metadata

from cloudflare_dyndns.app import app


def get_version() -> str:
    try:
        return importlib_metadata.version(__name__)
    except importlib_metadata.PackageNotFoundError:  # pragma: no cover
        return "unknown"


version: str = get_version()

__all__ = ["app", "get_version", "version"]

if __name__ == "main":
    app()  # pragma: no cover
