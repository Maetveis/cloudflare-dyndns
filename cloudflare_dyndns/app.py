from typing import Annotated, Any, ClassVar, Dict, Optional, cast

import copy
import enum
import importlib.resources
import io
import json
import logging
import socket
from dataclasses import dataclass
from ipaddress import IPv4Address, IPv6Address, ip_address
from pathlib import Path

import CloudFlare
import fastjsonschema
import requests
import rich.logging
import typer
import yaml
from requests_toolbelt.adapters.socket_options import SocketOptionsAdapter
from urllib3.util import Url
from xdg_base_dirs import xdg_cache_home, xdg_config_home

from .__adapters__ import RetryListHTTPAdapter

app = typer.Typer()


# Nonexistant URLs to use as a placeholder for the RetryListHTTPAdapter
IPV4_API_PLACEHOLDER = "https://maetveis.gitlab.io/cloudflare_dyndns/_loadbalancer/4"
UNIVERSAL_API_PLACEHOLDER = "https://maetveis.gitlab.io/cloudflare_dyndns/_loadbalancer/64"

IPV4_APIS = [
    "https://api.ipify.org",
    "https://4.ident.me",
    "https://4.tnedi.me",
    "https://ipv4.icanhazip.com",
]
UNIVERSAL_APIS = [
    "https://api64.ipify.org",
    "https://ident.me",
    "https://tnedi.me",
    "https://icanhazip.com",
    "https://ifconfig.me/ip",
]


# From include/linux/in6.h
IPV6_ADDR_PREFERENCES = 72

IPV6_PREFER_SRC_TMP = 0x0001
IPV6_PREFER_SRC_PUBLIC = 0x0002
IPV6_PREFER_SRC_PUBTMP_DEFAULT = 0x0100
IPV6_PREFER_SRC_COA = 0x0004
IPV6_PREFER_SRC_HOME = 0x0400
IPV6_PREFER_SRC_CGA = 0x0008
IPV6_PREFER_SRC_NONCGA = 0x0800

logger = logging.getLogger(__name__)


@dataclass
class Addresses(yaml.YAMLObject):
    ipv4: IPv4Address | None = None
    ipv6: IPv6Address | None = None
    yaml_loader = yaml.SafeLoader
    yaml_tag = "!Addresses"

    IPV4_YAML_TAG: ClassVar[str] = "!ipv4"
    IPV6_YAML_TAG: ClassVar[str] = "!ipv6"

    @classmethod
    def register_ip_serializers(cls) -> None:
        # Register the methods to (de)serialize IP addresses
        cls.yaml_loader.add_constructor(cls.IPV4_YAML_TAG, cls.ip_address_from_yaml)
        cls.yaml_loader.add_constructor(cls.IPV6_YAML_TAG, cls.ip_address_from_yaml)
        cls.yaml_dumper.add_representer(IPv4Address, cls.ip_address_to_yaml)
        cls.yaml_dumper.add_representer(IPv6Address, cls.ip_address_to_yaml)

    @classmethod
    def ip_address_to_yaml(cls, dumper: yaml.Dumper, data: IPv4Address | IPv6Address) -> Any:
        tag = cls.IPV4_YAML_TAG if isinstance(data, IPv4Address) else cls.IPV6_YAML_TAG
        return dumper.represent_scalar(tag, data.compressed)

    @classmethod
    def ip_address_from_yaml(
        cls, loader: yaml.Loader, node: yaml.Node
    ) -> IPv4Address | IPv6Address:
        assert isinstance(node, yaml.ScalarNode)
        value = loader.construct_scalar(node)
        if not isinstance(value, str):
            raise TypeError(f"Unexpected type: {type(value)}, expected: {str}")

        addr = ip_address(value)
        if isinstance(addr, IPv4Address):
            if node.tag != cls.IPV4_YAML_TAG:
                raise ValueError(f"Invalid tag: {node.tag}, expected: {cls.IPV4_YAML_TAG}")
            return IPv4Address(addr)

        assert isinstance(addr, IPv6Address)
        if node.tag != cls.IPV6_YAML_TAG:
            raise ValueError(f"Invalid tag: {node.tag}, expected: {cls.IPV6_YAML_TAG}")
        return IPv6Address(addr)

    @property
    def has_adress(self) -> bool:
        return self.ipv4 is not None or self.ipv6 is not None


Addresses.register_ip_serializers()


def resolve_ip(avoid_privacy_address: bool = True) -> Addresses:
    socket_options = copy.deepcopy(SocketOptionsAdapter.default_options)

    if avoid_privacy_address:
        socket_options.append((socket.IPPROTO_IPV6, IPV6_ADDR_PREFERENCES, IPV6_PREFER_SRC_PUBLIC))

    session = requests.Session()
    session.mount(IPV4_API_PLACEHOLDER, RetryListHTTPAdapter(replacement_urls=IPV4_APIS))
    session.mount(UNIVERSAL_API_PLACEHOLDER, RetryListHTTPAdapter(replacement_urls=UNIVERSAL_APIS))

    # TODO: Make timeouts configurable
    timeout = 30
    resp_text = session.get(UNIVERSAL_API_PLACEHOLDER, timeout=timeout).text.strip()
    universal_address = ip_address(resp_text)
    if isinstance(universal_address, IPv4Address):
        return Addresses(universal_address)

    resp_text = session.get(IPV4_API_PLACEHOLDER, timeout=timeout).text.strip()
    ipv4 = ip_address(resp_text)
    if not isinstance(ipv4, IPv4Address):
        raise RuntimeError("Expected API to return a valid IPV4 address")
    return Addresses(ipv4, IPv6Address(universal_address))


@dataclass
class Cache(yaml.YAMLObject):
    addresses: Addresses
    version: int = 1
    yaml_loader = yaml.SafeLoader
    yaml_tag = "!CacheDict"

    CACHE_FILE: ClassVar[Path] = Path("cloudflare_dyndns", "cache.yaml")
    CACHE_DISPLAY: ClassVar[str] = "/".join(("$XDG_CACHE_HOME", str(CACHE_FILE)))
    CACHE_DEFAULT = xdg_cache_home() / CACHE_FILE

    def verify(self) -> bool:
        return self.version == 1

    @classmethod
    def needs_to_update(cls, cache_file: Path, addresses: Addresses) -> bool:
        cache = cls.read(cache_file)
        if cache is None:
            return True

        return addresses != cache.addresses

    @classmethod
    def read(cls, cache_file: Path) -> "Cache | None":
        try:
            with cache_file.open() as stream:
                result = yaml.safe_load(stream)
                if not isinstance(result, Cache):
                    logger.warning(
                        "Failed to read cache, unexpected type: {}, expected: {}",
                        result.__class__,
                        Cache,
                    )
                    return None
                if not result.verify():
                    logger.warning("Unexpected cache format, ignoring.")
                    return None
                return result
        except FileNotFoundError:
            logger.debug("Cache file not found")
            return None
        except yaml.YAMLError as err:
            logger.warning("Failed to deserialize cache: {}", err)
            return None

    @classmethod
    def write(cls, cache_file: Path, addresses: Addresses) -> None:
        cache = Cache(addresses)
        logger.debug("Writing cache to %s", cache_file)
        cache_file.parent.mkdir(parents=True, exist_ok=True)
        with cache_file.open("w") as stream:
            yaml.dump(cache, stream)


@dataclass
class Config(yaml.YAMLObject):
    zone: str
    record_name: str
    remove_unresolved: bool
    fail_if_no_address: bool
    avoid_privacy_address: bool

    CONFIG_FILE: ClassVar[Path] = Path("cloudflare_dyndns", "config.yaml")
    CONFIG_DISPLAY: ClassVar[str] = "/".join(("$XDG_CONFIG_HOME", str(CONFIG_FILE)))
    CONFIG_DEFAULT: ClassVar[Path] = xdg_config_home() / CONFIG_FILE

    @classmethod
    def read(cls, stream: io.TextIOBase) -> "Config":
        schema_path = importlib.resources.files("cloudflare_dyndns") / "schemas/config.json"
        schema = json.load(schema_path.open(encoding="utf-8"))
        result: dict[str, Any] = yaml.safe_load(stream)
        fastjsonschema.validate(schema, result)
        return cls(**result)


@dataclass
class DnsRecord:
    identifier: str
    name: str
    content: str
    type: str


@dataclass
class RemoteRecords:
    ipv4: DnsRecord | None = None
    ipv6: DnsRecord | None = None


def get_remote_records(
    cf: CloudFlare, zone_id: str, addresses: Addresses, record_name: str
) -> RemoteRecords:
    remote_records = RemoteRecords()
    for attr, record_type in [("ipv4", "A"), ("ipv6", "AAAA")]:
        records = cf.zones.dns_records(zone_id, params={"type": record_type, "name": record_name})
        if len(records) <= 0:
            continue

        assert len(records) == 1
        r = records[0]
        setattr(remote_records, attr, DnsRecord(r["id"], r["name"], r["content"], r["type"]))
    return remote_records


def should_execute(msg: str, dry_run: bool = False) -> bool:
    logger.info(msg)
    return not dry_run


def update_or_create_records(
    cf: CloudFlare,
    zone_id: str,
    addresses: Addresses,
    remote_records: RemoteRecords,
    record_name: str,
    remove_unresolved: bool = True,
    force: bool = False,
    dry_run: bool = False,
) -> None:
    for attr, record_type in [("ipv4", "A"), ("ipv6", "AAAA")]:
        new_address = getattr(addresses, attr)
        remote_record = getattr(remote_records, attr)

        if new_address is None:
            if not remove_unresolved:
                continue
            if remote_record is None:
                logger.debug(f"Don't need to remove {record_type} record as it doesn't exists")
                continue

            if not should_execute(f"Remove {record_type}: {remote_record.content}", dry_run):
                continue
            cf.zones.dns_records.delete(zone_id, remote_record.identifier)
            continue

        if remote_record is not None:
            if remote_record.content == new_address.exploded:
                if force:
                    logger.info(f"Forcing update on up-to-date {record_type} record")
                else:
                    logger.debug(f"{record_type} record already matches")
                    continue

            if not should_execute(
                f"Update record {record_type}: {remote_record.content} -> {new_address.exploded}",
                dry_run,
            ):
                continue

            cf.zones.dns_records.patch(
                zone_id,
                remote_record.identifier,
                data={
                    "content": new_address.exploded,
                    "name": remote_record.name,
                    "type": remote_record.type,
                },
            )
            continue

        if not should_execute(f"Create record {record_type}: {new_address.exploded}", dry_run):
            continue

        cf.zones.dns_records.post(
            zone_id,
            data={"content": new_address.exploded, "name": record_name, "type": record_type},
        )


def update(
    addresses: Addresses, config: Config, force: bool = False, dry_run: bool = False
) -> None:
    cf = CloudFlare.CloudFlare()
    zone_id = cf.zones.get(params={"name": config.zone, "per_page": 1})[0]["id"]

    remote_records = get_remote_records(cf, zone_id, addresses, config.record_name)
    logger.debug(f"Received remote records: {remote_records}")
    update_or_create_records(
        cf,
        zone_id,
        addresses,
        remote_records,
        config.record_name,
        config.remove_unresolved,
        force,
        dry_run,
    )


class LogLevel(enum.StrEnum):
    DEBUG = "DEBUG"
    ERROR = "ERROR"
    INFO = "INFO"


@app.command()
def cli(
    log_level: LogLevel = LogLevel.INFO,
    config_file: Annotated[
        typer.FileText, typer.Option("--config-file", "-c", show_default=Config.CONFIG_DISPLAY)
    ] = cast(typer.FileText, Config.CONFIG_DEFAULT),
    cache_file: Annotated[
        Path, typer.Option("--cache-file", show_default=Cache.CACHE_DISPLAY)
    ] = Cache.CACHE_DEFAULT,
    force: Annotated[
        bool, typer.Option("--force", "-f", help="Force the update regardless of cached values")
    ] = False,
    dry_run: Annotated[
        bool,
        typer.Option(
            "--dry-run", "-n", help="Don't change any DNS entries just print what would be done"
        ),
    ] = False,
) -> None:
    logging.basicConfig(
        level=log_level.value,
        format="%(name)s: %(message)s",
        datefmt="[%X]",
        handlers=[rich.logging.RichHandler()],
    )

    logger.debug('Using config file: "%s"', str(config_file))
    config = Config.read(config_file)

    addresses = resolve_ip()
    if config.fail_if_no_address and not addresses.has_adress:
        logger.error("No external IP resolved")
        raise typer.Exit(1)

    logger.debug(f"Resolved IP to {addresses}")

    logger.debug('Checking for cached values in "%s"', cache_file)
    if not Cache.needs_to_update(cache_file, addresses) and not force:
        logger.info("Resolved IP address matches cache, skipping update")
        return

    update(addresses, config, force, dry_run)

    if not dry_run:
        Cache.write(cache_file, addresses)


if __name__ == "__main__":
    app()  # pragma: no cover
