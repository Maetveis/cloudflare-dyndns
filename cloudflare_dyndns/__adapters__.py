from typing import Any, TypeVar, cast
from typing_extensions import override

import copy
import functools
import logging
import random
import socket
from collections.abc import Sequence
from unittest.mock import patch

import requests
import requests.adapters
from requests.exceptions import RequestException, RetryError
from requests_toolbelt.adapters.socket_options import SocketOptionsAdapter
from urllib3.util.connection import _set_socket_options

logger = logging.getLogger(__name__)

T = TypeVar("T")


def _shuffled(lst: list[T]) -> list[T]:
    """Returns a copy of the list with the elements shuffled."""
    new = copy.copy(lst)
    random.shuffle(new)
    return new


class RetryListHTTPAdapter(SocketOptionsAdapter, requests.adapters.HTTPAdapter):
    def __init__(self, *args: Any, **kwargs: Any):
        self.urls = kwargs.pop("replacement_urls")
        super().__init__(*args, **kwargs)

    @override
    def send(
        self, request: requests.PreparedRequest, *args: Any, **kwargs: Any
    ) -> requests.Response:
        @functools.wraps(_set_socket_options)
        def set_socket_options(
            sock: socket.socket, socket_options: Sequence[tuple[int, int, int | bytes]] | None
        ) -> None:
            if socket_options is None:
                return

            fam_and_proto = [
                (socket.AF_INET, socket.IPPROTO_IP),
                (socket.AF_INET6, socket.IPPROTO_IPV6),
            ]
            for family, proto in fam_and_proto:
                if sock.family != family:
                    socket_options = [opt for opt in socket_options if opt[0] != proto]

            _set_socket_options(sock, socket_options)

        for url in _shuffled(self.urls):
            request.url = url
            try:
                logger.debug(f"Sending request to {url}")
                with patch("urllib3.util.connection._set_socket_options", set_socket_options):
                    resp = super().send(request, *args, **kwargs)
                resp.raise_for_status()
                return resp
            except ValueError as e:
                raise e
            except RequestException as e:
                logger.warning(f"Error from {url}: {e}")
                last_exception = e
        raise RetryError from last_exception
